/*
 *  This is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2015, Willem L, Kuylen E, Stijven S & Broeckhove J
 */

/**
 * @file
 * Implementation of the Simulator class.
 */

#include "Simulator.h"

#include "calendar/Calendar.h"
#include "calendar/DaysOffStandard.h"
#include "core/Cluster.h"
#include "core/ClusterType.h"
#include "core/Infector.h"
#include "core/LogMode.h"
#include "pop/Population.h"

#include <boost/property_tree/ptree.hpp>
#include <omp.h>
#include <memory>

#include <iostream>
#include <fstream>

namespace stride {

using namespace std;
using namespace boost::property_tree;
using namespace stride::util;

Simulator::Simulator()
        : m_config_pt(), m_num_threads(1U), m_log_level(LogMode::Null), m_population(nullptr),
          m_disease_profile(), m_track_index_case(false)
{
}

const shared_ptr<const Population> Simulator::GetPopulation() const
{
        return m_population;
}

void Simulator::SetTrackIndexCase(bool track_index_case)
{
        m_track_index_case = track_index_case;
}
///*********************************************///
///			TBB			///
///*********************************************///
#ifdef USE_TBB
template<LogMode log_level, bool track_index_case>
void Infector_Households::operator()( const tbb::blocked_range<size_t>& r ) const {
    for (size_t i = r.begin(); i < r.end(); i++) {
            Infector<log_level, track_index_case>::Execute(
                    sim->m_households[i], sim->m_disease_profile, sim->m_rng_handler[0], sim->m_calendar);
    }
};
    Infector_Households::Infector_Households( Simulator* _sim ) : sim(_sim){};

template<LogMode log_level, bool track_index_case>
void Infector_school_clusters::operator()( const tbb::blocked_range<size_t>& r ) const {
    for (size_t i = r.begin(); i < r.end(); i++) {
            Infector<log_level, track_index_case>::Execute(
                    sim->m_school_clusters[i], sim->m_disease_profile, sim->m_rng_handler[0], sim->m_calendar);
    }
};

Infector_school_clusters::Infector_school_clusters( Simulator* _sim ) : sim(_sim){};

template<LogMode log_level, bool track_index_case>
void Infector_m_work_clusters::operator()( const tbb::blocked_range<size_t>& r ) const {
    for (size_t i = r.begin(); i < r.end(); i++) {
            Infector<log_level, track_index_case>::Execute(
                    sim->m_work_clusters[i], sim->m_disease_profile, sim->m_rng_handler[0], sim->m_calendar);
    }
};
  
Infector_m_work_clusters::Infector_m_work_clusters( Simulator* _sim ) :sim(_sim){};


template<LogMode log_level, bool track_index_case>
void Infector_primary_community::operator()( const tbb::blocked_range<size_t>& r ) const {
    for (size_t i = r.begin(); i < r.end(); i++) {
            Infector<log_level, track_index_case>::Execute(
                    sim->m_primary_community[i], sim->m_disease_profile, sim->m_rng_handler[0], sim->m_calendar);
    }
};

    Infector_primary_community::Infector_primary_community( Simulator* _sim ) :	sim(_sim){};


template<LogMode log_level, bool track_index_case>
void Infector_secondary_community::operator()( const tbb::blocked_range<size_t>& r ) const {
	for (size_t i = r.begin(); i < r.end(); i++) {
		    Infector<log_level, track_index_case>::Execute(
		            sim->m_secondary_community[i], sim->m_disease_profile, sim->m_rng_handler[0], sim->m_calendar);
	}
}
Infector_secondary_community::Infector_secondary_community( Simulator* _sim ) :sim(_sim){};
#endif
template<LogMode log_level, bool track_index_case>
void Simulator::UpdateClusters()
{
#ifndef USE_TBB

        #pragma omp parallel num_threads(m_num_threads)
        {
                const unsigned int thread = omp_get_thread_num();

                #pragma omp for schedule(runtime)
                for (size_t i = 0; i < m_households.size(); i++) {
                        Infector<log_level, track_index_case>::Execute(
                                m_households[i], m_disease_profile, m_rng_handler[thread], m_calendar);
                }
                #pragma omp for schedule(runtime)
                for (size_t i = 0; i < m_school_clusters.size(); i++) {
                        Infector<log_level, track_index_case>::Execute(
                                m_school_clusters[i], m_disease_profile, m_rng_handler[thread], m_calendar);
                }
                #pragma omp for schedule(runtime)
                for (size_t i = 0; i < m_work_clusters.size(); i++) {
                        Infector<log_level, track_index_case>::Execute(
                                m_work_clusters[i], m_disease_profile, m_rng_handler[thread], m_calendar);
                }
                #pragma omp for schedule(runtime)
                for (size_t i = 0; i < m_primary_community.size(); i++) {
                        Infector<log_level, track_index_case>::Execute(
                                m_primary_community[i], m_disease_profile, m_rng_handler[thread], m_calendar);
                }
                #pragma omp for schedule(runtime)
                for (size_t i = 0; i < m_secondary_community.size(); i++) {
                        Infector<log_level, track_index_case>::Execute(
                                m_secondary_community[i], m_disease_profile, m_rng_handler[thread], m_calendar);
                }
        }
#else
    tbb::parallel_for(tbb::blocked_range<size_t>(0,m_households.size()), Infector_Households(this));
    tbb::parallel_for(tbb::blocked_range<size_t>(0,m_school_clusters.size()), Infector_school_clusters(this));
    tbb::parallel_for(tbb::blocked_range<size_t>(0,m_work_clusters.size()), Infector_m_work_clusters(this));
    tbb::parallel_for(tbb::blocked_range<size_t>(0,m_primary_community.size()), Infector_primary_community(this));
    tbb::parallel_for(tbb::blocked_range<size_t>(0,m_secondary_community.size()), Infector_secondary_community(this));
#endif
}

void Simulator::TimeStep()
{
        shared_ptr<DaysOffInterface> days_off {nullptr};

        // Logic where you compute (on the basis of input/config for initial day
        // or on the basis of number of sick persons, duration of epidemic etc)
        // what kind of DaysOff scheme you apply. If we want to make this cluster
        // dependent then the days_off object has to be passed into the Update function.
        days_off = make_shared<DaysOffStandard>(m_calendar);
        const bool is_work_off {days_off->IsWorkOff() };
        const bool is_school_off { days_off->IsSchoolOff() };

        for (auto& p : *m_population) {
                p.Update(is_work_off, is_school_off);
        }

        if (m_track_index_case) {
                switch (m_log_level) {
                        case LogMode::Contacts:
                                UpdateClusters<LogMode::Contacts, true>(); break;
                        case LogMode::Transmissions:
                                UpdateClusters<LogMode::Transmissions, true>(); break;
                        case LogMode::None:
                                UpdateClusters<LogMode::None, true>(); break;
                        default:
                                throw runtime_error(std::string(__func__) + "Log mode screwed up!");
                }
        } else {
                switch (m_log_level) {
                        case LogMode::Contacts:
                                UpdateClusters<LogMode::Contacts, false>(); break;
                        case LogMode::Transmissions:
                                UpdateClusters<LogMode::Transmissions, false>();  break;
                        case LogMode::None:
                                UpdateClusters<LogMode::None, false>(); break;
                        default:
                                throw runtime_error(std::string(__func__) + "Log mode screwed up!");
                }
        }

        m_calendar->AdvanceDay();
}

void Simulator::printVisFile(std::string filename) {
	ofstream file(filename);
	if (file.is_open()){
		file << "\"hh_id\",\"infected\"\n";
		for (int i = 1; i < m_households.size(); i++){
			file << i << "," << m_households[i].getInfectedCount() << "\n";
		}
	}
}
} // end_of_namespace
