################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Model/Cluster.cpp \
../src/Model/Household.cpp \
../src/Model/Model.cpp 

OBJS += \
./src/Model/Cluster.o \
./src/Model/Household.o \
./src/Model/Model.o 

CPP_DEPS += \
./src/Model/Cluster.d \
./src/Model/Household.d \
./src/Model/Model.d 


# Each subdirectory must supply rules for building sources it contributes
src/Model/%.o: ../src/Model/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -std=c++0x -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


