#ifndef VISPARSER_H_
#define VISPARSER_H_

/**
 * @file
 * Header for the VisParser class.
 */

#include <string>
#include <SFML/Graphics.hpp>

namespace Visualisation {

	class VisParser {

		/// Implemented as singleton

	public:

		static VisParser& getInstance();

		/// Basic getters
		int getWidth() const {
			return width;
		}
		int getHeight() const {
			return height;
		}
		int getX() const {
			return x;
		}
		int getY() const {
			return y;
		}
		std::string getFilename() const {
			return filename;
		}
		sf::Color getTextcolor() const {
			return textcolor;
		}
		sf::Color getHealthycolor() const {
			return healthycolor;
		}
		sf::Color getInfectedcolor() const {
			return infectedcolor;
		}

	private:
		VisParser();
		VisParser(VisParser const&);
		void operator=(VisParser const&);

		int width;								///< Holds the default window width
		int height;								///< Holds the default window height
		int x;									///< Holds the default number of cells on the x-axis
		int y;									///< Holds the default number of cells on the y-axis
		std::string filename;					///< Holds the default filename
		sf::Color textcolor;					///< Holds the default text color
		sf::Color healthycolor;					///< Holds the default health color
		sf::Color infectedcolor;				///< Holds the default infected color
	};

} /* namespace Visualisation */

#endif /* VISPARSER_H_ */
