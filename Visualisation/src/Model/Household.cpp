/**
 * @file
 * Implementation of the Household class.
 */

#include "Household.h"

namespace Visualisation {
	namespace Model {

		Household::Household(int id, double longitude, double latitude, int size) {
			this->id = id;
			this->longitude = longitude;
			this->latitude = latitude;
			this->size = size;
		}

		Household::~Household() {

		}

		void Household::setInfected(int inf){
			infected.push_back(inf);
		}

		bool Household::operator <(const Household& h) const{
			if (this->id < h.id){
				return true;
			}
			else {
				return false;
			}
		}

		bool Household::operator >(const Household& h) const{
			return (h<*this);
		}

		bool Household::operator <=(const Household& h) const{
			return !(*this>h);
		}

		bool Household::operator >=(const Household& h) const{
			return !(*this<h);
		}

		bool Household::operator ==(const Household& h) const{
			if (this->id == h.id){
				return true;
			}
			else {
				return false;
			}
		}

		bool Household::operator !=(const Household& h) const{
			return !(*this==h);
		}

	} /* namespace Model */
} /* namespace Visualisation */
