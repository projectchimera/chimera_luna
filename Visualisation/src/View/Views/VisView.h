#ifndef VISVIEW_H_
#define VISVIEW_H_

/**
 * @file
 * Header for the VisView class.
 */

#include "../../Model/Model.h"
#include "../View.h"

namespace Visualisation {
	namespace View {

		class VisView : public View {
		public:
			/// Default constructor
			VisView();
			/// Default destructor
			virtual ~VisView();

			/**
			 * Draw the model to the window
			 * Implementation of View::draw()
			 */
			virtual void draw();

			/**
			 * Sets the model for the view
			 * Also loads the background texture
			 * @param m				Pointer to the model to be used
			 */
			void setModel(const Model::Model* m);

		protected:
			const Model::Model* model;				///< Pointer to the model

			sf::Texture* texture;					///< Pointer to the background texture

		};

	}
}

#endif /* VISVIEW_H_ */
