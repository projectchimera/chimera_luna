/**
 * @file
 * Implementation of the GraphView class.
 */

#include "GraphView.h"
#include <vector>

namespace Visualisation {
	namespace View {

		GraphView::GraphView(int size) : View() {
			this->size = size;
		}

		GraphView::~GraphView() {

		}

		void GraphView::draw(){
			window->clear();

			// Create total population text
			sf::Text* totalpop = new sf::Text();
			std::string poptext = "Population in cell: ";
			poptext += std::to_string(cluster->getSize());
			totalpop->setString(poptext);
			totalpop->setFont(font);
			totalpop->setColor(visparser->getTextcolor());
			totalpop->setCharacterSize(50);
			sf::FloatRect popRect = totalpop->getLocalBounds();
			totalpop->setOrigin(popRect.left + popRect.width / 2.f, popRect.top + popRect.height / 2.f);
			totalpop->setPosition(250, 50);
			window->draw(*totalpop);
			delete totalpop;

			// Draw red rectangle around the graph
			sf::RectangleShape* r = new sf::RectangleShape();
			r->setOutlineColor(sf::Color::Red);
			r->setOutlineThickness(2);
			r->setFillColor(sf::Color::Transparent);
			r->setSize(sf::Vector2f(800,805));
			r->setPosition(sf::Vector2f(100,100));
			window->draw(*r);
			delete r;

			std::vector<int> infected = cluster->getInfected(size);

			// Create infected population text
			sf::Text* infpop = new sf::Text();
			std::string inftext = "Infected in cell: ";
			inftext += std::to_string(infected[cluster->getCurrentTimeStep()]);
			infpop->setString(inftext);
			infpop->setFont(font);
			infpop->setColor(visparser->getTextcolor());
			infpop->setCharacterSize(50);
			sf::FloatRect infRect = infpop->getLocalBounds();
			infpop->setOrigin(infRect.left + infRect.width / 2.f, infRect.top + infRect.height / 2.f);
			infpop->setPosition(750, 50);
			window->draw(*infpop);
			delete infpop;

			sf::Vertex line[] = {
				sf::Vertex(sf::Vector2f(100+cluster->getCurrentTimeStep()*800/size + 5,100)),
				sf::Vertex(sf::Vector2f(100+cluster->getCurrentTimeStep()*800/size + 5,900))
			};
			window->draw(line,2,sf::Lines);

			// Create circles for every step in the visualisation
			std::vector<sf::CircleShape> shapes;
			int max = 0;
			for (unsigned int i = 0; i < infected.size(); i++) {
				if (infected[i] > max) {
					max = infected[i];
				}
			}
			if (max == 0) {
				max = 10;
			}
			for (unsigned int i = 0; i < infected.size(); i++){
				sf::CircleShape shape;
				shape.setFillColor(sf::Color::Green);
				shape.setRadius(5);
				shape.setPosition(sf::Vector2f(100+i*800/size,900-800*infected[i]/max));
				shapes.push_back(shape);
			}

			// Create lines between the steps and draw them
			for (unsigned int i = 0; i < shapes.size()-1; i++){
				sf::Vertex line[] = {
						sf::Vertex(sf::Vector2f(shapes[i].getPosition().x + shapes[i].getRadius(),shapes[i].getPosition().y + shapes[i].getRadius())),
						sf::Vertex(sf::Vector2f(shapes[i+1].getPosition().x + shapes[i+1].getRadius(),shapes[i+1].getPosition().y + shapes[i+1].getRadius()))
				};
				window->draw(line,2,sf::Lines);
			}

			// Draw the circles (over the lines)
			for (unsigned int i = 0; i < shapes.size(); i++){
				window->draw(shapes[i]);
			}

			window->display();
		}

		void GraphView::setCluster(const Model::Cluster* c){
			this->cluster = c;
			c->subscribe(this);
		}

	} /* namespace View */
} /* namespace Visualisation */
