/**
 * @file
 * Implementation of the VisView class.
 */

#include "VisView.h"

#include <vector>
#include <iostream>

namespace Visualisation {
	namespace View {

		VisView::VisView() : View() {
			// Create empty texture
			texture = new sf::Texture();
		}

		VisView::~VisView() {

		}

		void VisView::draw(){
			window->clear();

			// Draw the background
			sf::RectangleShape shape;
			shape.setSize(sf::Vector2f(1000,1000));
			shape.setTexture(texture);
			window->draw(shape);

			// Draw the clusters!
			std::vector<Model::Cluster*> clusters = model->getClusters();
			for (unsigned int i = 0; i < clusters.size(); i++){
				window->draw(clusters[i]->getShape());
			}

			window->display();
		}

		void VisView::setModel(const Model::Model* m){
			this->model = m;
			m->subscribe(this);
			// Load backgroundtexture (or set default black background if file not found)
			if (!texture->loadFromFile(model->getTexture())){
				texture->loadFromFile("Resources/default.jpg");
				std::cerr << "Loading default black background instead." << std::endl;
			}
		}

	}

}
