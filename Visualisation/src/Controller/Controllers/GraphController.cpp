/**
 * @file
 * Implementation of the GraphController class.
 */

#include "GraphController.h"
#include <TGUI/TGUI.hpp>

#include "../../View/Views/GraphView.h"
#include "../../VisParser.h"

namespace Visualisation {
	namespace Controller {

		GraphController::GraphController(Model::Cluster* c, int size) : Controller() {
			this->size = size;
			// Set the cluster
			this->cluster = c;
			// Create a view and set it's cluster
			View::GraphView* view = new View::GraphView(size);
			view->setCluster(cluster);
			// Run the visualisation
			run();
			cluster->removeObs(view);

			// Cleanup
			delete view;
		}

		GraphController::~GraphController() {

		}

		void GraphController::draw() {
			// If cluster is updated , notify the view
			cluster->notify();
		}

		void GraphController::run() {
			cluster->update();
			while (window->isOpen()){
				sf::Event event;
				while (window->pollEvent(event)){
					switch (event.type) {
					case sf::Event::Closed: {
						cluster->resetTimeStep();
						window->close();
						break;
					}
					case sf::Event::KeyReleased: {
						switch (event.key.code) {
						// If escape is pressed, return to default controller
						case sf::Keyboard::Escape: {
							cluster->resetTimeStep();
							return;
							break;
						}
						// If h is pressed, open the help screen
						case sf::Keyboard::H: {
							help();
							break;
						}
						// If right arrow key is pressed, move timestep 1 position to the right
						case sf::Keyboard::Right: {
							if (cluster->getCurrentTimeStep() != size-1){
								cluster->incTimeStep();
							}
							break;
						}
						// If left arrow key is pressed, move timestep 1 position to the left
						case sf::Keyboard::Left: {
							if (cluster->getCurrentTimeStep() != 0){
								cluster->decTimeStep();
							}
							break;
						}
						default: {
							break;
						}
						}
						break;
					}
					default: {
						break;
					}
					}
				}
				draw();
			}
		}

		void GraphController::help() const {
			VisParser* visparser = &VisParser::getInstance();

			// Create window
			sf::RenderWindow window(sf::VideoMode(400,800), "Help");
			window.setFramerateLimit(60);

			// Create help text
			sf::Font font;
			font.loadFromFile("Resources/ADayinSeptember.otf");
			sf::Text* text = new sf::Text();
			text->setString("Welcome to the help screen for the Visualization tool.\n\n"
					"Controls:\n"
					"* Main vizualisation:"
					"   Space ------------ load next step.\n"
					"   Backspace -------- load previous step.\n"
					"   + (or p) --------- zoom in.\n"
					"   - (or m) --------- zoom out.\n"
					"   Left arrow ------- move screen left.\n"
					"   Right arrow ------ move screen right.\n"
					"   Up arrow --------- move screen up.\n"
					"   Down arrow ------- move screen down.\n"
					"   Escape ----------- close visualization.\n"
					"   h ---------------- open this help screen.\n"
					"   Left-clicking on any cell will open\n"
					"   the graph for that cell.\n"
					"* Graphs:\n"
					"   h ---------------- open this help screen.\n"
					"   Left arrow ------- move timestap left.\n"
					"   Right arrow ------ move timestap right.\n"
					"   Escape ----------- return to main visualization.\n\n"
					"Pressing the escape button will close this window.");
			text->setFont(font);
			text->setColor(visparser->getTextcolor());
			text->setCharacterSize(23);
			sf::FloatRect textRect = text->getLocalBounds();
			text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
			text->setPosition(200,400);

			while (window.isOpen()) {
				sf::Event event;
				while (window.pollEvent(event)) {
					switch(event.type){
					case sf::Event::Closed: {
						window.close();
						break;
					}
					case sf::Event::KeyReleased: {
						switch (event.key.code) {
						// If escape is pressed, close the window
						case sf::Keyboard::Escape: {
							window.close();
							break;
						}
						default: {
							break;
						}
						}
						break;
					}
					default: {
						break;
					}
					}
				}
				window.clear();
				// Draw the text to the window
				window.draw(*text);
				window.display();
			}
		}

	} /* namespace Controller */
} /* namespace Visualisation */
