/**
 * @file
 * Implementation of the Controller class.
 */
#include "Controller.h"

namespace Visualisation {
	namespace Controller {

		Controller::Controller() {
			// Set the window and its view
			window = &Window::getInstance();
			sf::View currentView(sf::Vector2f(500,500),sf::Vector2f(1000,1000));
			window->setView(currentView);
		}

		Controller::~Controller() {

		}

	} /* namespace Controller */
} /* namespace Visualisation */
