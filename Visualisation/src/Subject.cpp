/**
 * @file
 * Implementation of the Subject class.
 */

#include "Subject.h"

#include <algorithm>

namespace Visualisation {

	Subject::~Subject(){

	}

	void Subject::subscribe(Observer* obs) const {
		observers.emplace_back(obs);
	}

	void Subject::notify() {
		if (updated) {
			for (auto x : observers) {
				x->notify();
			}
		}
		updated = false;
	}

	void Subject::removeObs(Observer* obs){
		auto it = std::find(observers.begin(), observers.end(), obs);
		if (it != observers.end()){
			observers.erase(it);
		}
	}

}


