#ifndef OBSERVER_H_
#define OBSERVER_H_

/**
 * @file
 * Header for the Observer class.
 */

namespace Visualisation {

	class Observer {
	public:
		//Default destructor
		virtual ~Observer();
		//Pure virtual notify-method
		virtual void notify() = 0;

	};

}



#endif /* OBSERVER_H_ */
