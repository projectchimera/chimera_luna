/**
 * @file
 * Main file for the Visualization
 */

#include <iostream>
#include <fstream>
#include <string>

#include "Model/Model.h"
#include "Window.h"
#include "VisParser.h"
#include <TGUI/TGUI.hpp>
#include "Controller/Controllers/VisController.h"
#include <unistd.h>

using namespace Visualisation;

/// Run the actual visualization
void run(tgui::EditBox::Ptr w, tgui::EditBox::Ptr h, tgui::EditBox::Ptr x, tgui::EditBox::Ptr y, tgui::EditBox::Ptr file){
	// Create window with correct size
	VisParser* visparser;
	visparser = &VisParser::getInstance();
	std::string wi = w->getText().toAnsiString();
	int width;
	if (wi.size() == 0){
		width = visparser->getWidth();
	}
	else {
		width = stoi(wi);
	}
	std::string he = h->getText().toAnsiString();
	int height;
	if (he.size() == 0){
		height = visparser->getHeight();
	}
	else {
		height = stoi(he);
	}
	Window::getInstance().create(sf::VideoMode(width,height), "Visualization");
	// Create controller with correct settings
	std::string filename = file->getText().toAnsiString();
	if (filename.size() == 0){
		filename = visparser->getFilename();
	}
	std::string xs = x->getText().toAnsiString();
	int xsize;
	if (xs.size() == 0){
		xsize = visparser->getX();
	}
	else {
		xsize = stoi(xs);
	}
	std::string ys = y->getText().toAnsiString();
	int ysize;
	if (ys.size() == 0){
		ysize = visparser->getY();
	}
	else {
		ysize = stoi(ys);
	}
	Controller::VisController* controller = new Controller::VisController(xsize,ysize,filename);
	// Cleanup
	delete controller;
}

/// Controls for visualization UI
void visHelp2(tgui::Gui& gui,sf::Text* text){
	gui.removeAllWidgets();
	text->setString("Welcome to the help screen for the Visualization tool.\n\n"
			"Controls:\n"
			"* Main visualization:\n"
			"   Space ------------ load next step.\n"
			"   Backspace -------- load previous step.\n"
			"   + (or p) --------- zoom in.\n"
			"   - (or m) --------- zoom out.\n"
			"   Left arrow ------- move screen left.\n"
			"   Right arrow ------ move screen right.\n"
			"   Up arrow --------- move screen up.\n"
			"   Down arrow ------- move screen down.\n"
			"   Escape ----------- close visualization.\n"
			"   h ---------------- open this help screen.\n"
			"   Left-clicking on any cell will open\n"
			"   the graph for that cell.\n"
			"* Graphs:\n"
			"   Left arrow ------- move timestap left.\n"
			"   Right arrow ------ move timestap right.\n"
			"   h ---------------- open this help screen.\n"
			"   Escape ----------- return to main visualization.\n\n"
			"Pressing the escape button will close this window\n"
			"and you will return to the Visualization settings screen.");
	text->setPosition(200,400);
}

// Help for visualization UI
void visHelp1(){
	VisParser* visparser = &VisParser::getInstance();
	sf::RenderWindow window(sf::VideoMode(400,800), "Help");
	window.setFramerateLimit(60);
	sf::Font font;
	font.loadFromFile("Resources/ADayinSeptember.otf");
	sf::Text* text = new sf::Text();
	text->setString("Welcome to the help screen for the Visualization tool.\n\n"
			"To run the visualization, there are 2 options.\n"
			"1: Press the run-button without filling in any of the fields.\n"
			"This will run the visualization with the default settings,\n"
			"which you can find in the default.xml file in the resource folder.\n"
			"2: Fill in ALL the fields, and press the run button.\n"
			"This will run the visualization with the settings\n"
			"you provided in the fields.\n\n"
			"Explanation of the fields:\n"
			"* Width: the width of the window you want to run the visualization in.\n"
			"* Height: the height of the window you want to run the visualization in.\n"
			"* X: the amount of cells you want on the x-axis.\n"
			"* Y: the amount of cells you want on the y-axis.\n"
			"* File: the file you want the visualization to run on.\n\n"
			"Note 1: file needs to be in the resource folder.\n"
			"Note 2: example: to run pop_nassau_geo.csv, just write nassau as filename.\n"
			"Note 3: make sure you first ran stride on the same file!\n"
			"example: to visualize pop_nassau_geo.csv, run strinde on pop_nassau.csv .\n\n"
			"Pressing the next button will tell you more\n"
			"about the controls once you're running the visualization.\n\n"
			"Pressing the escape button will close this window\n"
			"and you will return to the visualization settings screen.");
	text->setFont(font);
	text->setColor(visparser->getTextcolor());
	text->setCharacterSize(23);
	sf::FloatRect textRect = text->getLocalBounds();
	text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
	text->setPosition(200,800/15*13/2);

	tgui::Gui gui(window);

	tgui::Button::Ptr next = tgui::Button::create();
	next->setSize(window.getSize().x/2, window.getSize().y/15);
	next->setPosition(window.getSize().x/4,window.getSize().y*13/15);
	next->setText("Next");
	gui.add(next);

	next->connect("pressed",visHelp2,std::ref(gui),text);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch(event.type){
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default: {
				break;
			}
			}
			gui.handleEvent(event);
		}
		window.clear();
		gui.draw();
		window.draw(*text);
		window.display();
	}
}

/// First (and only) step of the visualization
void loadVisWidgets(tgui::Gui& gui){
	auto windowWidth = tgui::bindWidth(gui);
	auto windowHeight = tgui::bindHeight(gui);

	tgui::EditBox::Ptr editBoxWidth = tgui::EditBox::create();
	editBoxWidth->setSize(windowWidth*2/3, windowHeight/15);
	editBoxWidth->setPosition(windowWidth/6,windowHeight/15);
	editBoxWidth->setDefaultText("Width");
	gui.add(editBoxWidth,"Width");

	tgui::EditBox::Ptr editBoxHeight = tgui::EditBox::create();
	editBoxHeight->setSize(windowWidth*2/3, windowHeight/15);
	editBoxHeight->setPosition(windowWidth/6,windowHeight*3/15);
	editBoxHeight->setDefaultText("Height");
	gui.add(editBoxHeight,"Width");

	tgui::EditBox::Ptr editBoxX = tgui::EditBox::create();
	editBoxX->setSize(windowWidth*2/3, windowHeight/15);
	editBoxX->setPosition(windowWidth/6,windowHeight*5/15);
	editBoxX->setDefaultText("X");
	gui.add(editBoxX,"X");

	tgui::EditBox::Ptr editBoxY = tgui::EditBox::create();
	editBoxY->setSize(windowWidth*2/3,windowHeight/15);
	editBoxY->setPosition(windowWidth/6,windowHeight*7/15);
	editBoxY->setDefaultText("Y");
	gui.add(editBoxY,"Y");

	tgui::EditBox::Ptr editBoxFile = tgui::EditBox::create();
	editBoxFile->setSize(windowWidth*2/3,windowHeight/15);
	editBoxFile->setPosition(windowWidth/6,windowHeight*9/15);
	editBoxFile->setDefaultText("File");
	gui.add(editBoxFile,"File");

	tgui::Button::Ptr button = tgui::Button::create();
	button->setSize(windowWidth/2,windowHeight/15);
	button->setPosition(windowWidth/4,windowHeight*11/15);
	button->setText("Run");
	gui.add(button);

	tgui::Button::Ptr help = tgui::Button::create();
	help->setSize(windowWidth/2, windowHeight/15);
	help->setPosition(windowWidth/4,windowHeight*13/15);
	help->setText("Help");
	gui.add(help);

	button->connect("pressed", run, editBoxWidth, editBoxHeight, editBoxX, editBoxY, editBoxFile);
	help->connect("pressed", visHelp1);
}

/// Run the visualization UI
void runVis() {
	sf::RenderWindow window(sf::VideoMode(400,600), "Visualization");
	tgui::Gui gui(window);

	try {
		loadVisWidgets(gui);
	}
	catch (const tgui::Exception& e) {
		std::cerr << "Failed to load TGUI widgets: " << e.what() << std::endl;
		return;
	}

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)){
			switch (event.type) {
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default: {
				break;
			}
			}
			gui.handleEvent(event);
		}
		window.clear();
		gui.draw();
		window.display();
	}
}

int id = 0;

/// Write the ending tags to the .xml file
void writeTags(tgui::Gui& gui){
	std::ofstream file;
	file.open("../PopulationGen/src/test.xml", std::ios_base::app);
	file << "  </Cities>" << std::endl;
	file << "</geoVerdelingsProfiel>" << std::endl;
	file.close();

	sf::RenderWindow* window = (sf::RenderWindow*)gui.getWindow();
	window->close();

	// Move current run-location to popgen location
	chdir("../PopulationGen");
	system("./Release/PopulationGen test.xml");
	std::cout << "Done generating population" << std::endl;
	// Set current run-location back to visualization location
	chdir("../Visualisation");
}

/// Help screen for the third popgen step
void popHelp3(){
	VisParser* visparser = &VisParser::getInstance();
	sf::RenderWindow window(sf::VideoMode(400,800), "Help");
	window.setFramerateLimit(60);
	sf::Font font;
	font.loadFromFile("Resources/ADayinSeptember.otf");
	sf::Text* text = new sf::Text();
	text->setString("Welcome to the help screen for the Population Generator!.\n\n"
			"This third (out of three) screen allows you to fill in the\n"
			"city options, like size of the city,\n"
			"and it's latitude and longitude.\n"
			"Clicking on next will add your city to the xml, and\n"
			"clear the fields, allowing you to add another city.\n"
			"Clicking on exit will run the population generator\n"
			"with the settings you provided\n"
			"Note: when clicking exit, the settings in the current\n"
			"      fields will NOT be saved!\n"
			"Make sure you fill in every field before clicking on next!\n\n"
			"Pressing the escape button will close this window\n"
			"and you will return to the Population generator.");
	text->setFont(font);
	text->setColor(visparser->getTextcolor());
	sf::FloatRect textRect = text->getLocalBounds();
	text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
	text->setPosition(200,400);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch(event.type){
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default: {
				break;
			}
			}
		}
		window.clear();
		window.draw(*text);
		window.display();
	}
}

/// Third step of the popgen (recursive version, also writing previous city to the .xml file)
void writeCity(tgui::Gui& gui, tgui::EditBox::Ptr lati, tgui::EditBox::Ptr longi, tgui::EditBox::Ptr sizer){
	std::ofstream file;
	file.open("../PopulationGen/src/test.xml", std::ios_base::app);
	file << "    <City>" << std::endl;
	file << "      <ID attribute1 = \"" << std::to_string(id) << "\"/>" << std::endl;
	file << "      <longitude attribute1 = \"" << longi->getText().toAnsiString() <<"\"/>" << std::endl;
	file << "      <latitude attribute1 = \"" << lati->getText().toAnsiString() << "\"/>" << std::endl;
	file << "      <size attribute1 = \"" << sizer->getText().toAnsiString() << "\"/>" << std::endl;
	file << "    </City>" << std::endl;
	file.close();
	id++;

	gui.removeAllWidgets();

	auto windowWidth = tgui::bindWidth(gui);
	auto windowHeight = tgui::bindHeight(gui);

	tgui::EditBox::Ptr latitude = tgui::EditBox::create();
	latitude->setSize(windowWidth*2/3, windowHeight/11);
	latitude->setPosition(windowWidth/6,windowHeight/11);
	latitude->setDefaultText("Latitude");
	gui.add(latitude,"Latitude");

	tgui::EditBox::Ptr longitude = tgui::EditBox::create();
	longitude->setSize(windowWidth*2/3, windowHeight/11);
	longitude->setPosition(windowWidth/6,windowHeight*3/11);
	longitude->setDefaultText("Longitude");
	gui.add(longitude,"Longitude");

	tgui::EditBox::Ptr size = tgui::EditBox::create();
	size->setSize(windowWidth*2/3, windowHeight/11);
	size->setPosition(windowWidth/6,windowHeight*5/11);
	size->setDefaultText("Size");
	gui.add(size,"Size");

	tgui::Button::Ptr next = tgui::Button::create();
	next->setSize(windowWidth/4,windowHeight/11);
	next->setPosition(windowWidth/8,windowHeight*7/11);
	next->setText("Next");
	gui.add(next);

	tgui::Button::Ptr exit = tgui::Button::create();
	exit->setSize(windowWidth/4,windowHeight/11);
	exit->setPosition(windowWidth*5/8,windowHeight*7/11);
	exit->setText("Exit");
	gui.add(exit);

	tgui::Button::Ptr help = tgui::Button::create();
	help->setSize(windowWidth/2,windowHeight/11);
	help->setPosition(windowWidth/4,windowHeight*9/11);
	help->setText("Help");
	gui.add(help);

	next->connect("pressed", writeCity, std::ref(gui), latitude, longitude, size);
	exit->connect("pressed", writeTags, std::ref(gui));
	help->connect("pressed", popHelp3);
}

/// Third step of the popgen
void loadPopgenWidgets3(tgui::Gui& gui) {
	gui.removeAllWidgets();

	auto windowWidth = tgui::bindWidth(gui);
	auto windowHeight = tgui::bindHeight(gui);

	tgui::EditBox::Ptr latitude = tgui::EditBox::create();
	latitude->setSize(windowWidth*2/3, windowHeight/11);
	latitude->setPosition(windowWidth/6,windowHeight/11);
	latitude->setDefaultText("Latitude");
	gui.add(latitude,"Latitude");

	tgui::EditBox::Ptr longitude = tgui::EditBox::create();
	longitude->setSize(windowWidth*2/3, windowHeight/11);
	longitude->setPosition(windowWidth/6,windowHeight*3/11);
	longitude->setDefaultText("Longitude");
	gui.add(longitude,"Longitude");

	tgui::EditBox::Ptr size = tgui::EditBox::create();
	size->setSize(windowWidth*2/3, windowHeight/11);
	size->setPosition(windowWidth/6,windowHeight*5/11);
	size->setDefaultText("Size");
	gui.add(size,"Size");

	tgui::Button::Ptr next = tgui::Button::create();
	next->setSize(windowWidth/4,windowHeight/11);
	next->setPosition(windowWidth/8,windowHeight*7/11);
	next->setText("Next");
	gui.add(next);

	tgui::Button::Ptr exit = tgui::Button::create();
	exit->setSize(windowWidth/4,windowHeight/11);
	exit->setPosition(windowWidth*5/8,windowHeight*7/11);
	exit->setText("Exit");
	gui.add(exit);

	tgui::Button::Ptr help = tgui::Button::create();
	help->setSize(windowWidth/2,windowHeight/11);
	help->setPosition(windowWidth/4,windowHeight*9/11);
	help->setText("Help");
	gui.add(help);

	next->connect("pressed", writeCity, std::ref(gui), latitude, longitude, size);
	exit->connect("pressed", writeTags, std::ref(gui));
	help->connect("pressed", popHelp3);
}

/// Write village information to the .xml file
void writeVillages(tgui::Gui& gui, tgui::EditBox::Ptr number, tgui::EditBox::Ptr minlat, tgui::EditBox::Ptr maxlat, tgui::EditBox::Ptr minlong, tgui::EditBox::Ptr maxlong){
	std::ofstream file;
	file.open("../PopulationGen/src/test.xml", std::ios_base::app);
	file << "  <Villages>" << std::endl;
	file << "    <totalNumber attribute1 = \"" << number->getText().toAnsiString() << "\"/>" << std::endl;
	file << "    <minLongitude attribute1 = \"" << minlong->getText().toAnsiString() <<"\"/>" << std::endl;
	file << "    <maxLongitude attribute1 = \"" << maxlong->getText().toAnsiString() << "\"/>" << std::endl;
	file << "    <minLatitude attribute1 = \"" << minlat->getText().toAnsiString() <<"\"/>" << std::endl;
	file << "    <maxLatitude attribute1 = \"" << maxlat->getText().toAnsiString() << "\"/>" << std::endl;
	file << "  </Villages>" << std::endl;
	file << "  <Cities>" << std::endl;
	file.close();
	loadPopgenWidgets3(std::ref(gui));
}

/// Help screen for the second popgen step
void popHelp2(){
	VisParser* visparser = &VisParser::getInstance();
	sf::RenderWindow window(sf::VideoMode(400,800), "Help");
	window.setFramerateLimit(60);
	sf::Font font;
	font.loadFromFile("Resources/ADayinSeptember.otf");
	sf::Text* text = new sf::Text();
	text->setString("Welcome to the help screen for the Population Generator!.\n\n"
			"This second (out of three) screen allows you to fill in the\n"
			"village options, like total number of villages,\n"
			"and their minimum and maximum latitude and longitude.\n"
			"Clicking on next will take you to the city options.\n"
			"For more help on the city options,\n"
			"click the help button in that screen.\n"
			"Make sure you fill in every field before clicking on next!\n\n"
			"Pressing the escape button will close this window\n"
			"and you will return to the Population generator.");
	text->setFont(font);
	text->setColor(visparser->getTextcolor());
	sf::FloatRect textRect = text->getLocalBounds();
	text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
	text->setPosition(200,400);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch(event.type){
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default: {
				break;
			}
			}
		}
		window.clear();
		window.draw(*text);
		window.display();
	}
}

/// Seconds step of the popgen
void loadPopgenWidgets2(tgui::Gui& gui) {
	gui.removeAllWidgets();

	auto windowWidth = tgui::bindWidth(gui);
	auto windowHeight = tgui::bindHeight(gui);

	tgui::EditBox::Ptr num = tgui::EditBox::create();
	num->setSize(windowWidth*2/3, windowHeight/15);
	num->setPosition(windowWidth/6,windowHeight/15);
	num->setDefaultText("Number of Villages");
	gui.add(num,"Number");

	tgui::EditBox::Ptr minlat = tgui::EditBox::create();
	minlat->setSize(windowWidth*2/3, windowHeight/15);
	minlat->setPosition(windowWidth/6,windowHeight*3/15);
	minlat->setDefaultText("Minimum latitude");
	gui.add(minlat,"Minlat");

	tgui::EditBox::Ptr maxlat = tgui::EditBox::create();
	maxlat->setSize(windowWidth*2/3, windowHeight/15);
	maxlat->setPosition(windowWidth/6,windowHeight*5/15);
	maxlat->setDefaultText("Maximum latitude");
	gui.add(maxlat,"Maxlat");

	tgui::EditBox::Ptr minlong = tgui::EditBox::create();
	minlong->setSize(windowWidth*2/3, windowHeight/15);
	minlong->setPosition(windowWidth/6,windowHeight*7/15);
	minlong->setDefaultText("Minimum longitude");
	gui.add(minlong,"Minlong");

	tgui::EditBox::Ptr maxlong = tgui::EditBox::create();
	maxlong->setSize(windowWidth*2/3, windowHeight/15);
	maxlong->setPosition(windowWidth/6,windowHeight*9/15);
	maxlong->setDefaultText("Maximum longitude");

	tgui::Button::Ptr button = tgui::Button::create();
	button->setSize(windowWidth/2,windowHeight/15);
	button->setPosition(windowWidth/4,windowHeight*11/15);
	button->setText("Next");
	gui.add(button);

	tgui::Button::Ptr help = tgui::Button::create();
	help->setSize(windowWidth/2,windowHeight/15);
	help->setPosition(windowWidth/4,windowHeight*13/15);
	help->setText("Help");
	gui.add(help);

	button->connect("pressed", writeVillages, std::ref(gui), num, minlat, maxlat, minlong, maxlong);
	help->connect("pressed", popHelp2);
}

/// Write settings to the .xml file
void writeSettings(tgui::Gui& gui, tgui::EditBox::Ptr size, tgui::CheckBox::Ptr coor){
	std::ofstream file;
	file.open("../PopulationGen/src/test.xml");
	file << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
	file << "<geoVerdelingsProfiel>" << std::endl;
	file << "  <Settings>" << std::endl;
	file << "    <populationSize attribute1 = \"" << size->getText().toAnsiString() << "\"/>" << std::endl;
	file << "    <coordinateSystem attribute1 = \"";
	if (coor->isChecked()) {
		file << "Realistic\"/>" << std::endl;
	}
	else {
		file << "Not\"/>" << std::endl;
	}
	file << "  </Settings>" << std::endl;
	file.close();
	loadPopgenWidgets2(std::ref(gui));
}

/// Help screen for the first popgen step
void popHelp1(){
	VisParser* visparser = &VisParser::getInstance();
	sf::RenderWindow window(sf::VideoMode(400,800), "Help");
	window.setFramerateLimit(60);
	sf::Font font;
	font.loadFromFile("Resources/ADayinSeptember.otf");
	sf::Text* text = new sf::Text();
	text->setString("Welcome to the help screen for the Population Generator!.\n\n"
			"This first (out of three) screen allows you to fill in the\n"
			"default options, like total population size\n"
			"and wether you want to use the coordinate\n"
			"system or not.\n"
			"Clicking on next will take you to the village options.\n"
			"For more help on the village options,\n"
			"click the help button in that screen.\n"
			"Make sure you fill in every field before clicking on next!\n\n"
			"Pressing the escape button will close this window\n"
			"and you will return to the Population generator.");
	text->setFont(font);
	text->setColor(visparser->getTextcolor());
	sf::FloatRect textRect = text->getLocalBounds();
	text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
	text->setPosition(200,400);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch(event.type){
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default: {
				break;
			}
			}
		}
		window.clear();
		window.draw(*text);
		window.display();
	}
}

/// First step of the popgen
void loadPopgenWidgets1(tgui::Gui& gui) {
	auto windowWidth = tgui::bindWidth(gui);
	auto windowHeight = tgui::bindHeight(gui);

	tgui::EditBox::Ptr editBoxSize = tgui::EditBox::create();
	editBoxSize->setSize(windowWidth*2/3, windowHeight/9);
	editBoxSize->setPosition(windowWidth/6,windowHeight/9);
	editBoxSize->setDefaultText("Population size");
	gui.add(editBoxSize,"Size");

	tgui::CheckBox::Ptr checkbox = tgui::CheckBox::create();
	checkbox->setPosition(windowWidth/6,windowHeight*3/9);
	checkbox->setText("Use coordinate system?");
	sf::Color color = checkbox->getRenderer()->getProperty("TextColor").getColor();
	checkbox->getRenderer()->setTextColorHover(color);
	checkbox->getRenderer()->setBackgroundColor(sf::Color::White);
	gui.add(checkbox);

	tgui::Button::Ptr button = tgui::Button::create();
	button->setSize(windowWidth/2,windowHeight/7);
	button->setPosition(windowWidth/4,windowHeight*5/9);
	button->setText("Next");
	gui.add(button);

	tgui::Button::Ptr help = tgui::Button::create();
	help->setSize(windowWidth/2,windowHeight/9);
	help->setPosition(windowWidth/4,windowHeight*7/9);
	help->setText("Help");
	gui.add(help);

	button->connect("pressed", writeSettings, std::ref(gui), editBoxSize, checkbox);
	help->connect("pressed", popHelp1);
}

/// Run the popgen UI
void runPopgen() {
	sf::RenderWindow window(sf::VideoMode(400,600), "Population Generator");
	tgui::Gui gui(window);

	try {
		loadPopgenWidgets1(gui);
	}
	catch (const tgui::Exception& e) {
		std::cerr << "Failed to load TGUI widgets: " << e.what() << std::endl;
		return;
	}

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)){
			switch (event.type) {
			case sf::Event::Closed: {
				window.close();
				break;
			}
			default: {
				break;
			}
			}
			gui.handleEvent(event);
		}
		window.clear();
		gui.draw();
		window.display();
	}
}

/// Run the main help screen
void mainHelp(){
	VisParser* visparser = &VisParser::getInstance();
	sf::RenderWindow window(sf::VideoMode(400,800), "Help");
	window.setFramerateLimit(60);
	sf::Font font;
	font.loadFromFile("Resources/ADayinSeptember.otf");
	sf::Text* text = new sf::Text();
	text->setString("Welcome to the help screen for the main menu.\n\n"
			"Clicking on the Population Generator button will\n"
			"open the population generator.\n"
			"For more help about the population generator,\n"
			"click the help button there.\n\n"
			"Clicking on the Visualization button will\n"
			"open the visualization tool.\n"
			"For more help about the visualization tool,\n"
			"click the help button there.\n\n"
			"Pressing the escape button will close this window\n"
			"and you will return to the main menu.");
	text->setFont(font);
	text->setColor(visparser->getTextcolor());
	sf::FloatRect textRect = text->getLocalBounds();
	text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
	text->setPosition(200,400);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch(event.type){
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default:{
				break;
			}
			}
		}
		window.clear();
		window.draw(*text);
		window.display();
	}
}

/**
 * Loads the basic widgets,
 * namely to open visualization or popgen
 * #param gui			the gui to load the widgets to
 */
void loadWidgets(tgui::Gui& gui) {
	auto windowWidth = tgui::bindWidth(gui);
	auto windowHeight = tgui::bindHeight(gui);

	tgui::Button::Ptr button1 = tgui::Button::create();
	button1->setSize(windowWidth/2,windowHeight/7);
	button1->setPosition(windowWidth/4,windowHeight*3/7);
	button1->setText("Visualization");
	gui.add(button1);

	tgui::Button::Ptr button2 = tgui::Button::create();
	button2->setSize(windowWidth/2,windowHeight/7);
	button2->setPosition(windowWidth/4,windowHeight*1/7);
	button2->setText("Population Generator");
	gui.add(button2);

	tgui::Button::Ptr help = tgui::Button::create();
	help->setSize(windowWidth/2, windowHeight/7);
	help->setPosition(windowWidth/4,windowHeight*5/7);
	help->setText("Help");
	gui.add(help);

	button1->connect("pressed", runVis);
	button2->connect("pressed", runPopgen);
	help->connect("pressed", mainHelp);
}

int main() {

	sf::RenderWindow window(sf::VideoMode(400,300), "Main menu");
	tgui::Gui gui(window);

	try {
		loadWidgets(gui);
	}
	catch (const tgui::Exception& e) {
		std::cerr << "Failed to load TGUI widgets: " << e.what() << std::endl;
		return 1;
	}

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)){
			switch (event.type) {
			case sf::Event::Closed: {
				window.close();
				break;
			}
			case sf::Event::KeyReleased: {
				switch (event.key.code) {
				// If escape is pressed, close the window
				case sf::Keyboard::Escape: {
					window.close();
					break;
				}
				default: {
					break;
				}
				}
				break;
			}
			default: {
				break;
			}
			}
			gui.handleEvent(event);
		}
		window.clear();
		gui.draw();
		window.display();
	}
	return 0;
}
